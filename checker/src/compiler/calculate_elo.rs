//! Calculates a lifter's Elo score over time

use crate::{AllMeetData, Entry, EntryIndex};
use opltypes::*;
use std::collections::HashMap;

extern crate csv;

pub type EloGroupMap = HashMap<String, Vec<(Username, Vec<EntryIndex>)>>;

// Store the Elo values that we have seen for a lifter and how many meets we
// have used to update each value.
pub type LifterEloMap = HashMap<String, (Points, u32)>;

pub fn get_meet_elo_group(entry: &Entry) -> String {
    entry.weightclasskg.to_string()
        + "_"
        + &entry.sex.to_string()
        + "_"
        + &entry.equipment.to_string()
        + "_"
        + &entry.event.to_string()
}

pub fn get_lifter_elo_group(entry: &Entry) -> String {
    entry.username.to_string()
        + "_"
        + &entry.equipment.to_string()
        + "_"
        + &entry.event.to_string()
}

/// Increments how many meets have been used to update a lifters Elo value
/// Has no action if a lifter does not have recorded Elo values
pub fn increment_lifter_meets(entry: &Entry, lifter_elo_map: &mut LifterEloMap) {
    let elo_group = get_lifter_elo_group(entry);
    if lifter_elo_map.contains_key(&elo_group) {
        let old_val = lifter_elo_map.get(&elo_group).unwrap().clone();
        let num_updates = old_val.1 + 1;
        lifter_elo_map.insert(elo_group, (old_val.0, num_updates));
    }
}

// Get the nonzero totals from the lifters data in a meet, if they have any
pub fn get_nonzero_total(
    curr_lifter: &Vec<EntryIndex>,
    meetdata: &AllMeetData,
) -> WeightKg {
    for idx in curr_lifter {
        let total = meetdata.get_entry(*idx).totalkg;
        if total.is_non_zero() {
            return total;
        }
    }
    WeightKg::from_i32(0)
}

/// Group lifters by weightclass, equipment, event and sex, ignoring division.
/// This is a vector of vectors to combine results where lifters competed in
/// multiple divisions.
pub fn group_lifters(meetdata: &AllMeetData, meet_idx: usize) -> EloGroupMap {
    let mut lifter_groups =
        EloGroupMap::with_capacity(meetdata.get_meet_len(EntryIndex::at(meet_idx, 0)));

    // Figure out how many meetdata a meet has
    for lifter_idx in 0..meetdata.get_meet_len(EntryIndex::at(meet_idx, 0)) {
        let entry_idx = EntryIndex::at(meet_idx, lifter_idx);

        let entry = meetdata.get_entry(entry_idx);
        let username = &entry.username;
        let elo_group = get_meet_elo_group(&entry);

        let group_lifter_vec =
            lifter_groups
                .entry(elo_group)
                .or_insert(Vec::<(Username, Vec<EntryIndex>)>::new());

        let mut lifter_idx = group_lifter_vec.iter().position(|x| x.0 == *username);

        if lifter_idx.is_none() {
            group_lifter_vec.push((username.clone(), Vec::<EntryIndex>::new()));
            lifter_idx = Some(group_lifter_vec.len() - 1);
        }

        let curr_lifter = &mut group_lifter_vec[lifter_idx.unwrap()];
        // Add to the list of entries for the current lifter.
        curr_lifter.1.push(entry_idx);
    }

    // Need to sort the lifter groups by total (with placing as the tiebreaker)
    for (_key, curr_group) in lifter_groups.iter_mut() {
        curr_group.sort_by(|b, a| {
            get_nonzero_total(&a.1, meetdata).cmp(&get_nonzero_total(&b.1, meetdata))
        });
    }
    lifter_groups
}

/// Get the learning rate to use for this matchup
// This just returns a constant right now
pub fn get_lifter_k_value(entry: &Entry, lifter_elo_map: &LifterEloMap) -> f32 {
    let elo_group_winner = entry.username.to_string()
        + "_"
        + &entry.equipment.to_string()
        + "_"
        + &entry.event.to_string();
    let learning_rate = 35.0;
    let mut num_updates_winner = 0;
    if let Some(old_val) = lifter_elo_map.get(&elo_group_winner) {
        num_updates_winner = old_val.1;
    }

    if num_updates_winner < 3 {
        learning_rate
    } else {
        learning_rate
    }
}

pub fn calc_new_elo(
    curr_elos: &Vec<Points>,
    curr_idx: usize,
    previous_elo: Points,
    entry: &Entry,
    lifter_elo_map: &LifterEloMap,
) -> Points {
    let score_scaling_factor = 400.0;
    let num_non_dq = curr_elos.len() as f32;
    let mov_correction = 1.0; // For accounting for the margin of victory in updating the Elo score, need to
                              // do some stats for this.
    let k_factor = get_lifter_k_value(entry, lifter_elo_map);

    if num_non_dq == 1.0 {
        return curr_elos[0];
    }

    let mut expectation_sum = 0.0;

    // The lifter we are calculating the expected score for.
    let curr_lifter_elo = curr_elos[curr_idx];

    for ii in 0..curr_elos.len() {
        if ii == curr_idx {
            continue;
        }

        let expected_term = 1.0
            / (1.0
                + 10.0_f32.powf(
                    (curr_elos[ii].to_f32() - curr_lifter_elo.to_f32())
                        / score_scaling_factor,
                ));
        expectation_sum += expected_term;
    }

    let expected_score = expectation_sum / (num_non_dq - 1.0);
    let actual_score = (num_non_dq - curr_idx as f32 - 1.0) / (num_non_dq as f32 - 1.0);

    Points::from(
        previous_elo.to_f32()
            + mov_correction * k_factor * (actual_score - expected_score),
    )
}

/// Given some entry for a lifter, update the Elo hash map for this lifter.
/// Returns the number of times this hash for the lifter has been updated
pub fn update_lifter_elo_hash(
    entry: &Entry,
    new_elo: Points,
    lifter_elo_map: &mut LifterEloMap,
) -> u32 {
    let elo_group = get_lifter_elo_group(entry);

    // If we already have a value for that elo type for the lifter update it and
    // increment the upate counter
    if lifter_elo_map.contains_key(&elo_group) {
        lifter_elo_map.get_mut(&elo_group).unwrap().0 = new_elo;
        lifter_elo_map.get(&elo_group).unwrap().1
    } else {
        lifter_elo_map.insert(elo_group, (new_elo, 1));
        1
    }
}

/// Update the Elo value for a lifter at a certain meet
pub fn update_elo_single_lifter(
    lifter: &Vec<EntryIndex>,
    previous_elo: Points,
    meetdata: &mut AllMeetData,
    lifter_elo_map: &mut LifterEloMap,
) {
    // Update the elo value hash
    update_lifter_elo_hash(
        meetdata.get_entry_mut(lifter[0]),
        previous_elo,
        lifter_elo_map,
    );

    for lifter_dup in lifter {
        let entry = meetdata.get_entry_mut(*lifter_dup);

        // We can't simply check this at function entry because it is possible that a
        // lifter is entered into two divisions for a meet and only DQs (or NSs) one of
        // them Powerlifting India has done this in a few cases.
        if entry.totalkg.is_non_zero() {
            entry.skill = previous_elo;
        } else {
            // Default value is zero, just setting it here to be sure.
            entry.skill = Points::from(0.0);
        }
    }
}

/// Returns the Elo of the last result where the lifter didn't bomb, if there is
/// no such result return 1000
pub fn get_previous_elo(
    lifter_idx: &EntryIndex,
    lifter_elo_map: &LifterEloMap,
    meetdata: &AllMeetData,
) -> Points {
    let entry = meetdata.get_entry(*lifter_idx);
    let elo_group = entry.username.to_string()
        + "_"
        + &entry.equipment.to_string()
        + "_"
        + &entry.event.to_string();

    match lifter_elo_map.get(&elo_group) {
        Some(value) => value.0,
        None => Points::from(1000.0),
    }
}

/// Loops over all meets and calculates a lifters Elo score
pub fn calculate_elo(meetdata: &mut AllMeetData) {
    // Store a map of Elo values hashed by lifter name, Equipment, Event
    let mut lifter_elo_map = LifterEloMap::with_capacity(800_000);

    // Sort meets by date first
    let mut meet_idcs = (0..meetdata.get_meets().len()).collect::<Vec<usize>>();
    meet_idcs.sort_by(|a, b| {
        meetdata
            .get_meet(EntryIndex::at(*a, 0))
            .date
            .cmp(&meetdata.get_meet(EntryIndex::at(*b, 0)).date)
    });

    // Loop over meets by date, updating the Elo value for each lifter based on the
    // new competition data
    for ii in meet_idcs {
        let elo_groups = group_lifters(meetdata, ii);

        for (_key, curr_lifters) in &elo_groups {
            // Count the number of non DQ lifters
            let mut num_non_dq = 0;
            for lifter in curr_lifters {
                if get_nonzero_total(&lifter.1, meetdata).is_non_zero() {
                    num_non_dq += 1;
                }
            }

            let mut prev_elos = Vec::<Points>::with_capacity(num_non_dq);
            let mut new_elos = Vec::<Points>::with_capacity(num_non_dq);

            // Update the lifter Elos with the new values
            for ii in 0..num_non_dq {
                let curr_lifter_data = &curr_lifters[ii];
                prev_elos.push(get_previous_elo(
                    &curr_lifter_data.1[0],
                    &lifter_elo_map,
                    meetdata,
                ));
            }

            // Calculate the expected result for the current lifter
            for (ii, lifter_meet_data) in curr_lifters.iter().enumerate() {
                // Check if the lifter is DQ
                if ii < num_non_dq {
                    let curr_entry = meetdata.get_entry(lifter_meet_data.1[0]);
                    let new_elo = calc_new_elo(
                        &prev_elos,
                        ii,
                        prev_elos[ii],
                        &curr_entry,
                        &lifter_elo_map,
                    );

                    new_elos.push(new_elo);
                } else {
                    // Increase the bomb probability for a lifter

                }
            }
            // Update the lifter Elos with the new values
            for (ii, lifter_meet_data) in curr_lifters[0..num_non_dq].iter().enumerate() {
                update_elo_single_lifter(
                    &curr_lifters[ii].1,
                    new_elos[ii],
                    meetdata,
                    &mut lifter_elo_map,
                );
                // Increment the meet counter for the curent lifter
                increment_lifter_meets(
                    meetdata.get_entry(lifter_meet_data.1[0]),
                    &mut lifter_elo_map,
                );
            }
        }
    }
}

// Need to actually write some tests...
#[cfg(test)]
mod tests {
    use super::*;
    use NarrowResult::{Conflict, Integrated};

}
