//! Contains Rocket logic for "distributions" of the site, like OpenIPF.

pub mod openipf;
pub mod powerelo;
